$(document).foundation();

$(document).ready(function() {
    $('#fullpage').fullpage({
      scrollOverflow: true,
      normalScrollElements: '#hot-jobs, #all',
      normalScrollElementTouchThreshold: 10,
      onLeave: function(index, nextIndex, direction){
        var leavingSection = $(this);

        //using index
        if(nextIndex == 1){
          $('header').addClass('expanded');
		  $('.alert-banner').show();
        } else {
          $('header').removeClass('expanded');
		  $('.alert-banner').hide();
        }

      },
      afterRender: function(){
          $('header').addClass('expanded');
          if ( $('#step5 .fp-scrollable').length > 0 ) {
          	$('#step5 .fp-tableCell').css('padding', 0);
          } else {
          	$('#step5 .fp-tableCell').removeAttr('style');          	
          }

          var padding = $('header').outerHeight() + $('#top').outerHeight();
          $('.home #step1 #boxes').css('padding-top', padding);

      }
    });

    $('.menu-icon').click(function(){
      $('#slide-menu').toggleClass('open');
    });

    $('#slide-menu #close').click(function(){
      $('#slide-menu').removeClass('open');
      return false;
    });

    $('.tab').hide();
    var tab = $('.tab-nav .active a').attr('href');
    $(tab).show();

    $('.tab-nav a').click(function(){
    	var tab = $(this).attr('href');
    	$('.tab-nav li').removeClass('active');
    	$(this).parent('li').addClass('active');
    	$('.tab').hide();
    	$(tab).show().find('.career-detail').removeAttr('style').find('.columns').height('auto');
    	equalheight('.tab .career-list .career-detail > .columns');
    	return false;
    });

    $('#found-careers > div').hide();
    $('#hot-jobs > div').hide();
    $('#all > div').hide();
    var activeview = $('#sort i.active').data('link');
    $(activeview).show();

    $('#sort i').click(function(){
        $('#sort i').removeClass('active');
        $(this).addClass('active');
        var activeview = $(this).data('link');
        $('#found-careers > div').hide();
        $('#hot-jobs > div').hide();
        $('#all > div').hide();
        $(activeview).show();
    });

    $(document).on('click', '.next .icon-down, .next.first-section', function(){
		  $.fn.fullpage.moveSectionDown();
		});
		$(document).on('click', '.next .icon-up', function(){
		  $.fn.fullpage.moveSectionUp();
		});

		$(".question-group").hide().eq(0).show().addClass('active');

		/* normal mode
		$('.question-group input[type="checkbox"]').change(function(){
			var qid = $('.question-group').index( $(this).closest('.question-group') );

			if ($('.question-group.active input[type="checkbox"]:checked').length > 0) {
				$('.progress-bar li').eq(qid).addClass('is-complete').find('.progress-line').addClass('in-progress');
				$('.show-for-medium .progress-bar li').eq(qid).addClass('is-complete').find('.progress-line').addClass('in-progress');
			} else {
				$('.progress-bar li').eq(qid).removeClass('is-complete').find('.progress-line').removeClass('in-progress');
				$('.show-for-medium .progress-bar li').eq(qid).removeClass('is-complete').find('.progress-line').removeClass('in-progress');
			}
		});

		// mobile mode
		$('.question-group .group-2 input[type="checkbox"]').change(function(){
			var qid = $('.question-group').index( $(this).closest('.question-group') );
			if ($('.question-group.active .group-2 input[type="checkbox"]:checked').length > 0) {
				$('.progress-bar li').eq(qid).find('.progress-line').addClass('is-complete');
			} else {
				$('.progress-bar li').eq(qid).find('.progress-line').removeClass('is-complete');
			}
		}); */

		$('#next-question').click(function(){
			var current = $('.question-group.active').eq(0);
			var nextQid = $('.question-group').index( current ) + 1;

			if( $('.progress-bar-mobile').is(':visible') ) {
				if ( $('.question-group.active .group-2').length > 0 && !$('.question-group.active .group-2').is(':visible') ) {
					$('.question-group.active .group-2').show();
					$('.question-group.active .group-1').hide();
				} else {
					current.removeClass('active').hide().next().addClass('active').show();
                    $('.progress-bar li.is-active').removeClass('is-active').addClass('is-complete');
					$('.progress-bar li').eq(nextQid).addClass('is-active');
				}
			} else {
				current.removeClass('active').hide().next().addClass('active').show();
                $('.show-for-medium .progress-bar li.is-active').removeClass('is-active').addClass('is-complete');
				$('.show-for-medium .progress-bar li').eq(nextQid).addClass('is-active');
			}
		
		});

		$('.progress-bar li').click(function(){
			$('.progress-bar li').removeClass('is-active').removeClass('is-complete');
			$('.question-group .group-2').removeAttr('style');
			$(this).addClass('is-active');
			var qid = $(this).data('step');
			$(".question-group").hide().removeClass('active').find('.group-1').show();
			$('#question-'+qid).addClass('active').show();
            $( "li" ).each(function( index ) {
              if ($(this).data('step') <= qid) {
                $(this).addClass('is-complete');
              }
            });
		});

    // added on 10.5.2017
    
    $('#boot-camp-table .map_btn').on('click', function(){
      //var mapViewText=jQuery(this).text();
      //alert(mapViewText);
      $(this).css({'display':'none'});
      $('.list_btn').css({'display':'block'});
      $('#boot-camp-table .list-view').css({'display':'none'}); 
      $('#boot-camp-table .map-view').css({'display':'block'}); 
    });

    $('#boot-camp-table .list_btn').on('click', function(){
      //var mapViewText=jQuery(this).text();
      //alert(mapViewText);
      $(this).css({'display':'none'});
      $('.map_btn').css({'display':'block'});
      
      $('#boot-camp-table .list-view').css({'display':'block'}); 
      $('#boot-camp-table .map-view').css({'display':'none'}); 
    });

    $('.service-data .map_btn').on('click', function(){
       //alert('hi')
       $(this).css({'display':'none'});
       $(this).siblings('.service-data .list_btn').css({'display':'block'});
       $(this).parent().parent().parent().siblings('.list-view').css({'display':'none'});
       $(this).parent().parent().parent().siblings('.map-view').css({'display':'block'});
    });

    $('.service-data .list_btn').on('click', function(){
       //alert('hi')
       $(this).css({'display':'none'});
       $(this).siblings('.service-data .map_btn').css({'display':'block'});
       $(this).parent().parent().parent().siblings('.map-view').css({'display':'none'});
       $(this).parent().parent().parent().siblings('.list-view').css({'display':'block'});
    });


// ********************************* END
});




$(window).load(function() {
  equalheight('.tab .career-list .career-detail > .columns');
  $(".question-group").setAllToMaxHeight();
});

$(window).resize(function(){
  equalheight('.tab .career-list .career-detail > .columns');
  $(".question-group").setAllToMaxHeight();
});

$.fn.setAllToMaxHeight = function(){
	return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
}

equalheight = function(container){

	var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;

	$(container).each(function() {

	$el = $(this);
	$($el).height('auto')
	topPostion = $el.position().top;

	  if (currentRowStart != topPostion) {
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			 rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
	  } else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	    rowDivs[currentDiv].height(currentTallest);
	  }
 	});
}

var __slice = [].slice;

// Contact Us radio show and hide
function question() {
	if (document.getElementById('ssQuestion').checked) {
		document.getElementById('form_ssq').style.display = 'block';
		document.getElementById('form_acq').style.display = 'none';
	}
	else {
		document.getElementById('form_ssq').style.display = 'none';
		document.getElementById('form_acq').style.display = 'block';
	}
}
	
// Add multiple select / deselect functionality
$(function(){
	// Army All
	$("#army_all").click(function () {
		  $('input[name="army_all"]').attr('checked', this.checked);
	});
	$('input[name="army_all"]').click(function(){

		if($('input[name="army_all"]').length == $('input[name="army_all"]:checked').length) {
			$("#army_all").attr("checked", "checked");
		} else {
			$("#army_all").removeAttr("checked");
		}
	});
	
	// Marine Corps All
	$("#marine_corps_all").click(function () {
		  $('input[name="marine_corps_all"]').attr('checked', this.checked);
	});
	$('input[name="marine_corps_all"]').click(function(){

		if($('input[name="marine_corps_all"]').length == $('input[name="marine_corps_all"]:checked').length) {
			$("#marine_corps_all").attr("checked", "checked");
		} else {
			$("#marine_corps_all").removeAttr("checked");
		}
	});
	
	// Air Force All
	$("#air_force_all").click(function () {
		  $('input[name="air_force_all"]').attr('checked', this.checked);
	});
	$('input[name="air_force_all"]').click(function(){

		if($('input[name="air_force_all"]').length == $('input[name="air_force_all"]:checked').length) {
			$("#air_force_all").attr("checked", "checked");
		} else {
			$("#air_force_all").removeAttr("checked");
		}
	});
	
	// Navy All
	$("#navy_all").click(function () {
		  $('input[name="navy_all"]').attr('checked', this.checked);
	});
	$('input[name="navy_all"]').click(function(){

		if($('input[name="navy_all"]').length == $('input[name="navy_all"]:checked').length) {
			$("#navy_all").attr("checked", "checked");
		} else {
			$("#navy_all").removeAttr("checked");
		}
	});
	
	// Coast Guard All
	$("#coast_guard_all").click(function () {
		  $('input[name="coast_guard_all"]').attr('checked', this.checked);
	});
	$('input[name="coast_guard_all"]').click(function(){

		if($('input[name="coast_guard_all"]').length == $('input[name="coast_guard_all"]:checked').length) {
			$("#coast_guard_all").attr("checked", "checked");
		} else {
			$("#coast_guard_all").removeAttr("checked");
		}
	});
});